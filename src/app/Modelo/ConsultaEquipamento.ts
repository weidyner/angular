export class ConsultaEquipamento{
    id:number;
    tipo:String;
    marca:String;
    modelo:String;
    numeroSerie:String;
}