import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Equipamento } from '../Modelo/Equipamento';
import { Marca } from '../Modelo/Marca';
import { Tipo } from '../Modelo/Tipo';
import { Modelo } from '../Modelo/Modelo';
import { ConsultaEquipamento } from '../Modelo/ConsultaEquipamento';

/*
@Injectable({
  providedIn: 'root'
})
*/
@Injectable()
export class ServiceService {

  
  constructor(private http:HttpClient) { }

  Url='http://localhost:8080/equipamentos';

  UrlMarca='http://localhost:8080/marca';

  UrlTipo='http://localhost:8080/tipos';

  UrlModelo='http://localhost:8080/modelos';

  UrlConsulta='http://localhost:8080/consultaEquipamentos';

  getEquipamentos(){
    return this.http.get<Equipamento[]>(this.Url);
  }
  createEquipamento(equipamento:Equipamento){
    return this.http.post<Equipamento>(this.Url, equipamento);
  }
  getEquipamentoId(id:number){
    return this.http.get<Equipamento>(this.Url+"/"+id);
  }
  updateEquipamento(equipamento:Equipamento){
    return this.http.put<Equipamento>(this.Url+"/"+equipamento.id, equipamento);
  }
  deleteEquipamento(equipamento:Equipamento){
    return this.http.delete<Equipamento>(this.Url+"/"+equipamento.id);
  }
  //Marcas
  getMarcas(){
    return this.http.get<Marca[]>(this.UrlMarca);
  }
  getMarcaId(id:number){
    return this.http.get<Marca>(this.UrlMarca+"/"+id);
  }
  //Tipos
  getTipos(){
    return this.http.get<Tipo[]>(this.UrlTipo);
  }
  getTipoId(id:number){
    return this.http.get<Tipo>(this.UrlTipo+"/"+id);
  }
  //Modelos
  getModelos(){
    return this.http.get<Modelo[]>(this.UrlModelo);
  }
  getModeloId(id:number){
    return this.http.get<Modelo>(this.UrlModelo+"/"+id);
  }
  //Consulta
  getConsultaEquipamentos(){
    return this.http.get<ConsultaEquipamento[]>(this.UrlConsulta);
  }
}
