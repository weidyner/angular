import { Component } from '@angular/core';
//import { allowedNodeEnvironmentFlags } from 'process';
//import { ListarComponent } from './Equipamento/listar/listar.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  ngOnInit(): void {
    this.Listar();
  }

  title = 'projetoInventario';
  termFilter='';
  constructor(private router:Router){}

    Listar(){
      this.router.navigate(["listar"]);
    }
    Novo(){
      this.router.navigate(["add"]);
    }
  
}
