import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Equipamento } from 'src/app/Modelo/Equipamento';
import { ServiceService } from 'src/app/Service/service.service';
import { Marca } from 'src/app/Modelo/Marca';
import { Tipo } from 'src/app/Modelo/Tipo';
import { Modelo } from 'src/app/Modelo/Modelo';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  equipamento:Equipamento=new Equipamento();
  marcas:Marca[];
  tipos:Tipo[];
  modelos:Modelo[];
  constructor(private router:Router, private service:ServiceService) { }

  ngOnInit(): void {
    this.Editar();

    this.service.getMarcas()
    .subscribe(data=>{
      this.marcas=data;
    })
    this.service.getTipos()
    .subscribe(data=>{
      this.tipos=data;
    })

    this.service.getModelos()
    .subscribe(data=>{
      this.modelos=data;
    })
  }

  Editar(){
    let id=localStorage.getItem("id");
    this.service.getEquipamentoId(+id)
    .subscribe(data=>{
      this.equipamento=data;
    })
  }
  Atualizar(equipamento:Equipamento){
    this.service.updateEquipamento(equipamento)
    .subscribe(data=>{
      this.equipamento=data;
      alert("Atualizado com sucesso.");
      this.router.navigate(["listar"]);
    })
  }
  Listar(){
    this.router.navigate(["listar"]);
  }
}
