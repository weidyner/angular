import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../../Service/service.service';
import { Router } from '@angular/router';
import { Equipamento } from '../../Modelo/Equipamento';
import { Marca } from '../../Modelo/Marca';
import { Tipo } from '../../Modelo/Tipo';
import { Modelo } from '../../Modelo/Modelo';
import { ConsultaEquipamento } from '../../Modelo/ConsultaEquipamento';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { identifierName } from '@angular/compiler';



@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {
  termFilter='';
  equipamentos:Equipamento[];
  marcas:Marca[];
  tipos:Tipo[];
  modelos:Modelo[];
  consultaEquipamentos:ConsultaEquipamento[];
  constructor(private service:ServiceService, private router:Router) { }
  
  ngOnInit(): void {
    this.Listar();

    this.service.getEquipamentos()
    .subscribe(data=>{
        this.equipamentos=data;
    })

    this.service.getMarcas()
    .subscribe(data=>{
      this.marcas=data;
    })

    this.service.getTipos()
    .subscribe(data=>{
      this.tipos=data;
    })

    this.service.getModelos()
    .subscribe(data=>{
      this.modelos=data;
    })

    this.service.getConsultaEquipamentos()
    .subscribe(data=>{
        this.consultaEquipamentos=data;
    })
  }
  Editar(equipamento:Equipamento):void{
    localStorage.setItem("id",equipamento.id.toString());
    this.router.navigate(["edit"]);
  }
  Delete(equipamento:Equipamento){
    this.service.deleteEquipamento(equipamento)
    .subscribe(data=>{
      this.equipamentos=this.equipamentos.filter(e=>e!==equipamento);
      alert("Equipamento excluído com sucesso");
      window.location.reload();
    })
  }
  Novo(){
    this.router.navigate(["add"]);
  }
  Listar(){
    this.router.navigate(["listar"]);
  }
  public paginaAtual = 1;
  
}
