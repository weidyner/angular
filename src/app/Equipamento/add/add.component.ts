import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Equipamento } from 'src/app/Modelo/Equipamento';
import { ServiceService } from 'src/app/Service/service.service';
import { Marca } from 'src/app/Modelo/Marca';
import { Tipo } from 'src/app/Modelo/Tipo';
import { Modelo } from 'src/app/Modelo/Modelo';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  
  equipamento:Equipamento=new Equipamento();
  marcas:Marca[];
  tipos:Tipo[];
  modelos:Modelo[];
  constructor(private router:Router, private service:ServiceService) { }

  ngOnInit(): void {
    this.service.getMarcas()
    .subscribe(data=>{
      this.marcas=data;
    })
    this.service.getTipos()
    .subscribe(data=>{
      this.tipos=data;
    })

    this.service.getModelos()
    .subscribe(data=>{
      this.modelos=data;
    })
  }

  Salvar(){
    this.service.createEquipamento(this.equipamento)
    .subscribe(data=>{
      console.log(this.equipamento);
      alert("Salvo com sucesso");
      this.router.navigate(["listar"]);
    })
  }
  Listar(){
    this.router.navigate(["listar"]);
  }

}


